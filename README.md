# Online-Shopping

Rest-api for online shopping cart.User can sign up and login.When user sign-up,a cart is created for him.
He can add product to his cart,can view different items in the cart ,can delete items in the cart.
Basic authentication is applied to see user is logged in or not.
Technologies used: java,jax-rs,mysql,maven.